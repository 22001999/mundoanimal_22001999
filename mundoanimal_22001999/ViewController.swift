//
//  ViewController.swift
//  mundoanimal_22001999
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher


struct MundoAnimal: Decodable {
    let image_link: String
    let latin_name : String
    let name : String
    
}


class ViewController: UIViewController {
    

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nav_name: UINavigationItem!
    @IBOutlet weak var label_latin: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMundoAnimal()
        
    }
    
    func getMundoAnimal(){
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: MundoAnimal.self) { response in
                if let animal = response.value{
                    self.imageView.kf.setImage(with: URL(string: animal.image_link))
                    self.label_latin.text = animal.latin_name
                    self.nav_name.title = animal.name
                }
            }
        }



}

